class Word < ActiveRecord::Base
  validates :value, presence: true
  validates :value, uniqueness: { case_sensitive: false }

  self.per_page = 10
end
