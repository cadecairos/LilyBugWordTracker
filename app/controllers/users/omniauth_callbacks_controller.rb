class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook

    auth = request.env["omniauth.auth"]

    if !Rails.configuration.x.allowed_users.include? auth.info.email
      flash.alert = "You are not authorized to log into this app"
      redirect_to new_user_session_url
    else
      @user = User.from_omniauth(auth)


      if @user.persisted?
        sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
        set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
      else
        session["devise.facebook_data"] = request.env["omniauth.auth"]
        redirect_to new_user_registration_url
      end
    end
  end

  def failure
    redirect_to root_path
  end
end
