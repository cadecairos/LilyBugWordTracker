class WordsController < ApplicationController
  before_action :authenticate_user!
  before_action :has_id, only: :destroy
  before_action :validate_word, only: :create

  def index
    if params[:search]
      @words = Word.page(params[:page]).where('value LIKE ?', "%#{params[:search]}%").order('value asc')
    else
      @words = Word.page(params[:page]).order('value asc')
    end
  end

  def create
    if @word.save
      redirect_to :root
    else
      oops
    end
  end

  def new
    render "new"
  end

  def destroy
    if params[:id].nil?
      oops
    else
      word = Word.find_by id: params[:id]
      if word.destroy
        redirect_to :root
      else
        oops
      end
    end
  end

  def oops
    render "oops"
  end

  def dupe
    render "dupe"
  end

  private

  def

  def has_id
    if params[:id].nil?
      oops
    end
  end

  def haz_value
    if params[:word][:value].nil?
      oops
    end
  end

  def validate_word
    @word = Word.new value: params[:word][:value]

    if @word.invalid?
      dupe
    end
  end
end
