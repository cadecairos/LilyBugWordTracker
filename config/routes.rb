Rails.application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  resources :words, only: [:new, :create, :destroy]

  root 'words#index'

  if Rails.env.development?
    get '/oops', to: 'words#oops'
    get '/dupe', to: 'words#dupe'
  end
end
