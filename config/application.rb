require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)

module LilyWordCounter
  class Application < Rails::Application
    config.x.allowed_users = ['chris@chrisdecairos.ca']

    config.active_record.raise_in_transactional_callbacks = true
  end
end
